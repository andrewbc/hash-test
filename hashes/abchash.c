#ifndef ABCHASH_H
#include "abchash.h"
#endif

#include <stddef.h>
#include <stdio.h>
// Code below is covered by the Paul Hsieh OLD BSD license as seen at
// http://www.azillionmonkeys.com/qed/weblicense.html
// and http://www.azillionmonkeys.com/qed/hash.html
//
// Copyright (c) 2010, Paul Hsieh
// All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

// Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// Neither my name, Paul Hsieh, nor the names of any other contributors to the code use may not be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define get16bits(d) (* ((uint16_t *)(d)) )

uint32_t ABCHash(const unsigned char *data, uint32_t len) {
    if (data == NULL || len == 0) return 0;
    // I've modified this function just a little bit.
    // Pray I do not alter it further. - ABC
    uint32_t hash = len;
    uint32_t rem = len & 3;
    len >>= 2;

    /* Main loop */
    while (len > 0) {
        hash  += get16bits(data);
        hash   = (hash << 16) ^ ((get16bits(data+2) << 11) ^ hash);
        hash  += hash >> 11;
        data  += 4;
        --len;
    }

    /* Handle end cases */
    switch (rem) {
        case 3: hash += get16bits(data);
                hash ^= hash << 16;
                hash ^= data[2] << 18;
                hash += hash >> 11;
                break;
        case 2: hash += get16bits(data);
                hash ^= hash << 11;
                hash += hash >> 17;
                break;
        case 1: hash += *data;
                hash ^= hash << 10;
                hash += hash >> 1;
                break;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}

uint32_t ABCHash64(const unsigned char *restrict data, uint32_t len) {
    if (data == NULL || len == 0) return 0;
    // I've modified this function even more than the one above.
    // Pray I do not alter it further. - ABC
    uint16_t t1;
    uint16_t t2;
    uint16_t t3;
    uint16_t t4;
    uint32_t hash = len;
    uint32_t rem = len & 7;
    len >>= 3;

    /* Main loop */
    while (len>0) {
        t1 = get16bits(data);
        t2 = get16bits(data+2);
        t3 = get16bits(data+4);
        t4 = get16bits(data+6);
        hash  += t1;
        hash   = (hash << 16) ^ ((t2 << 11) ^ hash);
        hash  += hash >> 11;
        hash  += t3;
        hash   = (hash << 16) ^ ((t4 << 11) ^ hash);
        hash  += hash >> 11;
        data  += 8;
        --len;
    }

    /* Handle end cases */
    switch (rem) {
        case 7:
            hash += data[6];
            hash ^= hash << 12;
            hash += hash >> 1;
        case 6:
            hash ^= data[5] << 7;
            hash ^= hash << 10;
            hash += hash >> 18;
        case 5:
            hash += data[4];
            hash ^= hash << 9;
        case 4:
            hash += data[3] << 11;
            hash ^= hash << 7;
        case 3:
            hash ^= data[2] << 18;
            hash += hash >> 11;
        case 2:
            hash += data[1];
            hash ^= hash << 11;
            hash += hash >> 17;
        case 1:
            hash += data[0];
            hash ^= hash << 10;
            hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}
