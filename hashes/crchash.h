/*
 *	This code was found at: http://wannabe.guru.org/alg/node191.html
 *  and still exists here: http://www.fearme.com/misc/alg/node191.html
 *
 *  this source code is based on Rex and Binstock which, in turn,
 *  acknowledges William James Hunt.
 *
 *	According to the site this CRC uses the polynomial x^16+x^5+x^2+1.
 *	Unfortunately, DOCSIS uses x^16+x^12+x^5+1.  D'oh!
 */

static uint32_t crc_16_table[16] = {
    0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401,
    0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400
};

static uint32_t GetCRC16Update(uint32_t start_crc, const unsigned char * data_stream, int length) {
    uint32_t crc = start_crc;
    uint32_t r;

    /* while there is more data to process */
    while (length-- > 0) {

        /* compute checksum of lower four bits of *data_stream */
        r = crc_16_table[crc & 0xF];
        crc = (crc >> 4) & 0x0FFF;
        crc ^= r ^ crc_16_table[*data_stream & 0xF];

        /* now compute checksum of upper four bits of *data_stream */
        r = crc_16_table[crc & 0xF];
        crc = (crc >> 4) & 0x0FFF;
        crc ^= r ^ crc_16_table[(*data_stream >> 4) & 0xF];

        /* next... */
        data_stream++;
    }
    return crc;
}

uint32_t GetCRC16(const unsigned char * data_stream, int length) {
	return GetCRC16Update (0, data_stream, length);
}

/* ======================================================================== */

static uint32_t crc_table[256];

/*  This code was found at:
 *  http://cell.onecall.net/cell-relay/publications/software/CRC/32bitCRC.c.html
 */

/*                                                                      */
/* crc32h.c -- package to compute 32-bit CRC one byte at a time using   */
/*             the high-bit first (Big-Endian) bit ordering convention  */
/*                                                                      */
/* Synopsis:                                                            */
/*  gen_crc_table() -- generates a 256-word table containing all CRC    */
/*                     remainders for every possible 8-bit byte.  It    */
/*                     must be executed (once) before any CRC updates.  */
/*                                                                      */
/*  unsigned update_crc(crc_accum, data_blk_ptr, data_blk_size)         */
/*           unsigned crc_accum; char *data_blk_ptr; int data_blk_size; */
/*           Returns the updated value of the CRC accumulator after     */
/*           processing each byte in the addressed block of data.       */
/*                                                                      */
/*  It is assumed that an unsigned long is at least 32 bits wide and    */
/*  that the predefined type char occupies one 8-bit byte of storage.   */
/*                                                                      */
/*  The generator polynomial used for this version of the package is    */
/*  x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x^1+x^0 */
/*  as specified in the Autodin/Ethernet/ADCCP protocol standards.      */
/*  Other degree 32 polynomials may be substituted by re-defining the   */
/*  symbol POLYNOMIAL below.  Lower degree polynomials must first be    */
/*  multiplied by an appropriate power of x.  The representation used   */
/*  is that the coefficient of x^0 is stored in the LSB of the 32-bit   */
/*  word and the coefficient of x^31 is stored in the most significant  */
/*  bit.  The CRC is to be appended to the data most significant byte   */
/*  first.  For those protocols in which bytes are transmitted MSB      */
/*  first and in the same order as they are encountered in the block    */
/*  this convention results in the CRC remainder being transmitted with */
/*  the coefficient of x^31 first and with that of x^0 last (just as    */
/*  would be done by a hardware shift register mechanization).          */
/*                                                                      */
/*  The table lookup technique was adapted from the algorithm described */
/*  by Avram Perez, Byte-wise CRC Calculations, IEEE Micro 3, 40 (1983).*/

/* generate the table of CRC remainders for all possible bytes */

#define CRC32POLYNOMIAL 0x04c11db7L

static void GenerateCRC32Table(void) {
    register int i, j;
    register uint32_t crc_accum;

	for ( i = 0;  i < 256;  i++ ) {
		crc_accum = ( (unsigned long) i << 24 );
        for ( j = 0;  j < 8;  j++ ) {
			if ( crc_accum & 0x80000000L ) {
				crc_accum = ( crc_accum << 1 ) ^ CRC32POLYNOMIAL;
			} else {
				crc_accum = ( crc_accum << 1 );
			}
		}
		crc_table[i] = crc_accum;
	}
	return;
}

/* update the CRC on the data block one byte at a time */

static uint32_t UpdateCRC32(uint32_t crc_accum, const unsigned char *data_blk_ptr, int data_blk_size) {
    register int j;
    register uint8_t i;

	for (j = 0; j < data_blk_size; j++) {
		i = (crc_accum >> 24) ^ *data_blk_ptr++;
		crc_accum = (crc_accum << 8) ^ crc_table[i];
	}
	return crc_accum;
}

uint32_t GetCRC32 (const unsigned char * data_stream, int length) {
	return UpdateCRC32 (0, data_stream, length);
}

/* ======================================================================== */

/* Performs two parallel CRC-32 on even and odd bytes of the input, then
   combines the two in a further CRC-32 calculation */

uint32_t GetCRC32PH (const unsigned char *data_blk_ptr, int data_blk_size) {
int j;
uint8_t i0, i1;
uint32_t crc_accum0 = 0, crc_accum1 = UINT32_C (0x23456789);

	if (data_blk_size & 1) crc_accum0 ^= *data_blk_ptr++;
	for (j = 1; j < data_blk_size; j+=2) {
		i0 = ((crc_accum0 >> 24) ^ *data_blk_ptr++);
		i1 = ((crc_accum1 >> 24) ^ *data_blk_ptr++);
		crc_accum0 = (crc_accum0 << 8) ^ crc_table[i0];
		crc_accum1 = (crc_accum1 << 8) ^ crc_table[i1];
	}
	return crc_accum0 + crc_accum1;
}
