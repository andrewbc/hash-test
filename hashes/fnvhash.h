/* Fowler / Noll / Vo (FNV) Hash

   http://www.isthe.com/chongo/tech/comp/fnv/ */

uint32_t FNVHash (const unsigned char * data, int len) {
int i;
uint32_t hash;

	hash = UINT32_C(2166136261);
	for (i=0; i < len; i++) {
		hash = (UINT32_C(16777619) * hash) ^ data[i];
	}
	return hash;
}
