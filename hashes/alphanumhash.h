/* A hashing function that I believe was created by either Chris Torek or
   Dan Bernstein */

uint32_t alphaNumHash (const unsigned char * s, int len) {
    uint32_t h;
    int i;

    for (h = 0, i = 0; i < len; i++) {
        h = (h << 5) + (h * 5) + (unsigned char) s[i];
    }
    return h;
}
