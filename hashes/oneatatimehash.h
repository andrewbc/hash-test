/*
 * http://burtleburtle.net/bob/hash/doobs.html
 */

uint32_t oneAtATimeHash (const unsigned char * s, int len) {
int32_t hash;
int i;

	for (hash = 0, i = 0; i < len; i++) {
		hash += s[i];
		hash += (hash << 10);
		hash ^= (hash >>  6);	/* Non-portable due to ANSI C */
	}
	hash += (hash <<  3);
	hash ^= (hash >> 11);		/* Non-portable due to ANSI C */
	hash += (hash << 15);
	return (uint32_t) hash;
}

/* ======================================================================== */

uint32_t oneAtATimeHashPH (const unsigned char * s, int len) {
int32_t hash0 = 0, hash1 = INT32_C(0x23456789);
int i;

	if (len & 1) hash1 ^= *s++;

	for (i = 1; i < len; i+=2) {
		hash0 += *s++;
		hash1 += *s++;
		hash0 += (hash0 << 10);
		hash1 += (hash1 << 10);
		hash0 ^= (hash0 >>  6);	/* Non-portable due to ANSI C */
		hash1 ^= (hash1 >>  6);	/* Non-portable due to ANSI C */
	}

	hash0 += hash1;

	hash0 += (hash0 <<  3);
	hash0 ^= (hash0 >> 11);		/* Non-portable due to ANSI C */
	hash0 += (hash0 << 15);
	return (uint32_t) hash0;
}
