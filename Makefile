ROOT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

CFILES = $(shell find . -iname '*.c')

all: hashtest

clean:
	rm -rf build
	mkdir build

hashtest:
	clang -lm -std=c99 -O3 -fstrict-aliasing -W -Wall -Wextra -Werror -Wstrict-aliasing -Wno-unused-label $(CFILES) -o build/hashtest_clang
	gcc -std=c99 -O3 -fstrict-aliasing -W -Wall -Wextra -Werror -Wstrict-aliasing -Wno-unused-label $(CFILES) -lm -o build/hashtest_gcc
	clang -lm -std=c99 -O0 -fstrict-aliasing -W -Wall -Wextra -Werror -Wstrict-aliasing -Wno-unused-label $(CFILES) -o build/hashtest_clang_d
	gcc -std=c99 -O0 -fstrict-aliasing -W -Wall -Wextra -Werror -Wstrict-aliasing -Wno-unused-label $(CFILES) -lm -o build/hashtest_gcc_d
