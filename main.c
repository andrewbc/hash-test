#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

// Hash tests for various hash functions. Welcome to the hash gauntlet.
#include "hashes/abchash.h"
#include "hashes/alphanumhash.h"
#include "hashes/bobhash.h"
#include "hashes/crchash.h"
#include "hashes/fnvhash.h"
#include "hashes/oneatatimehash.h"
#include "hashes/superfasthash.h"

// Most of the tests below were ported from bob jenkins'
// http://burtleburtle.net/bob/c/lookup3.c
// lookup3.c, by Bob Jenkins, May 2006, Public Domain.

// testSpeed was ported from Paul Hsieh's tests here
// http://www.azillionmonkeys.com/qed/hash.c
// http://www.azillionmonkeys.com/qed/hash.html
// under his derivative license found here
// http://www.azillionmonkeys.com/qed/weblicense.html

//========================================================================
// If you make a hash function with a different call type, add below and
// make sure you alter tests to do it right.
//========================================================================
typedef uint32_t (* normHashFn) (const unsigned char *s, int len);
typedef uint32_t (* abcHashFn) (const unsigned char *s, uint32_t len);
typedef uint32_t (* bob2HashFn) (const void *s, size_t len, uint32_t initval);

typedef union {
    normHashFn  hash0;
    abcHashFn   hash1;
    bob2HashFn  hash2;
} funcPoly;

typedef struct {
    unsigned int funcType;
    funcPoly func;
    uint64_t maskValue;
    char *name;
} hash_t;

int verbose=0;
int quiet=0;

//========================================================================
// Speed testing
//========================================================================
#ifndef CLOCKS_PER_SEC
# ifdef CLK_TCK
#  define CLOCKS_PER_SEC (CLK_TCK)
# endif
#endif

#define BUFF_SZ 256
#define RUNS 5
#define NHASHES 5000000

void SpeedsWithBuffer(hash_t *hashes, const unsigned char *buff) {
    clock_t c0, c1;
    uint32_t i=0;
    uint32_t j=0;
    uint32_t t_id=0;
    double results[RUNS];
    double worst;
    double total;
    double best;

    // all possible hash function types
    normHashFn hash0;
    abcHashFn hash1;
    bob2HashFn hash2;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        worst = 0.0;
        best = 0.0;
        total = 0.0;
        hash_t *h = &hashes[t_id];
        printf("%12s:", h->name);
        fflush(stdout);
        switch(h->funcType) {
            case 0:
                hash0 = h->func.hash0;
                for (i=0; i < RUNS; i++) {
                    c0 = clock();
                    for (j=0; j < NHASHES; j++) {
                        hash0(buff, BUFF_SZ);
                    }
                    c1 = clock();
                    results[i] = (c1 - c0) * (1.0 / (double)CLOCKS_PER_SEC);
                    if (results[i] > worst) {
                        worst = results[i];
                    }
                    if (best == 0.0 || results[i] < best) {
                        best = results[i];
                    }
                    total += results[i];
                }
                break;
            case 1:
                hash1 = h->func.hash1;
                for (i=0; i < RUNS; i++) {
                    c0 = clock();
                    for (j=0; j < NHASHES; j++) {
                        hash1(buff, BUFF_SZ);
                    }
                    c1 = clock();
                    results[i] = (c1 - c0) * (1.0 / (double)CLOCKS_PER_SEC);
                    if (results[i] > worst) {
                        worst = results[i];
                    }
                    if (best == 0.0 || results[i] < best) {
                        best = results[i];
                    }
                    total += results[i];
                }
                break;
            case 2:
                hash2 = h->func.hash2;
                for (i=0; i < RUNS; i++) {
                    c0 = clock();
                    for (j=0; j < NHASHES; j++) {
                        hash2(buff, BUFF_SZ, 0);
                    }
                    c1 = clock();
                    results[i] = (c1 - c0) * (1.0 / (double)CLOCKS_PER_SEC);
                    if (results[i] > worst) {
                        worst = results[i];
                    }
                    if (best == 0.0 || results[i] < best) {
                        best = results[i];
                    }
                    total += results[i];
                }
                break;
            default:
                fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", h->funcType, t_id, h->name);
                exit(1);
                break;
        }

        if (!quiet) printf(" [ %6.4fs %7.4fs %7.4fs %7.4fs %7.4fs ]",results[0], results[1], results[2], results[3], results[4]);
        printf(" %7.4fs worst %7.4fs average %7.4fs best\n" , worst, total/RUNS, best);
    }
}

void testSpeed(hash_t *hashes) {
	printf("Time in seconds to hash a random buffer of %u bytes %u times, with %u runs.\n", BUFF_SZ, NHASHES, RUNS);
    static unsigned char buff[BUFF_SZ];
    buff[0]=0;
    for (int i=1; i < BUFF_SZ; i++) { buff[i] = (char) (i + buff[i-1]); }
    SpeedsWithBuffer(hashes, buff);
}

void testSpeed2(hash_t *hashes) {
	printf("\nTime in seconds to hash a homogenous buffer of %u bytes %u times, with %u runs.\n", BUFF_SZ, NHASHES, RUNS);
    static unsigned char buff[BUFF_SZ];
    for (int i=0; i<BUFF_SZ; ++i) buff[i] = 'x';
    SpeedsWithBuffer(hashes, buff);
}

//========================================================================
// Avalanche testing
//========================================================================
#define HASHSTATE 1
#define HASHLEN   1
#define MAXPAIR 60
#define MAXLEN  70
void testAvalanche(hash_t *hashes) {
    printf("\nCheck Avalanche. (that every input bit changes every output bit half the time)\n");
    printf("No more than %d trials should ever be needed.\n",MAXPAIR/2);

    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("%s\n", hasher->name);
        uint8_t qa[MAXLEN+1], qb[MAXLEN+2], *a = &qa[0], *b = &qb[1];
        uint32_t c[HASHSTATE], d[HASHSTATE], i=0, j=0, k, l, m=0, z;
        uint32_t e[HASHSTATE],f[HASHSTATE],g[HASHSTATE],h[HASHSTATE];
        uint32_t x[HASHSTATE],y[HASHSTATE];
        uint32_t hlen;

        for (hlen=0; hlen < MAXLEN; ++hlen) {
            z = 0;
            for (i=0; i < hlen; ++i) {// for each input byte
                for (j=0; j < 8; ++j) { // for each input bit
                    for (l=0; l < HASHSTATE; ++l) e[l]=f[l]=g[l]=h[l]=x[l]=y[l]=~((uint32_t)0);
                    //---- check that every output bit is affected by that input bit
                    for (k=0; k < MAXPAIR; k+=2) {
                        uint32_t finished = 1;
                        // keys have one bit different
                        for (l=0; l < hlen+1; ++l) { a[l] = b[l] = (uint8_t)0; }
                        // have a and b be two keys differing in only one bit
                        a[i] ^= (k<<j);
                        a[i] ^= (k>>(8-j));
                        switch(hasher->funcType) {
                            case 0:
                                c[0] = hasher->func.hash0(a, hlen);
                                break;
                            case 1:
                                c[0] = hasher->func.hash1(a, hlen);
                                break;
                            case 2:
                                c[0] = hasher->func.hash2(a, hlen, 0);
                                break;
                            default:
                                fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                                exit(1);
                                break;
                        }
                        b[i] ^= ((k+1)<<j);
                        b[i] ^= ((k+1)>>(8-j));
                        switch(hasher->funcType) {
                            case 0:
                                d[0] = hasher->func.hash0(b, hlen);
                                break;
                            case 1:
                                d[0] = hasher->func.hash1(b, hlen);
                                break;
                            case 2:
                                d[0] = hasher->func.hash2(b, hlen, 0);
                                break;
                            default:
                                fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                                exit(1);
                                break;
                        }
                        // check every bit is 1, 0, set, and not set at least once
                        for (l=0; l < HASHSTATE; ++l) {
                            e[l] &= (c[l]^d[l]);
                            f[l] &= ~(c[l]^d[l]);
                            g[l] &= c[l];
                            h[l] &= ~c[l];
                            x[l] &= d[l];
                            y[l] &= ~d[l];
                            if (e[l]|f[l]|g[l]|h[l]|x[l]|y[l]) finished = 0;
                        }
                        if (finished) break;
                    }
                    if (k > z) z = k;
                    if (k == MAXPAIR) {
                        printf("Some bit didn't change: ");
                        printf("%.8x %.8x %.8x %.8x %.8x %.8x  ",e[0],f[0],g[0],h[0],x[0],y[0]);
                        printf("i %d j %d m %d len %d\n", i, j, m, hlen);
                    }
                    if (z == MAXPAIR) goto done;
                }
            }
            done:
            if (z < MAXPAIR) {
                printf("Mix success  %2d bytes  %2d initvals  ",i,m);
                printf("required  %d  trials\n", z/2);
            }
        }
        printf("\n");
    }
}


//========================================================================
// Bounds and Alignment testing
//========================================================================
void testBoundsAndAlignment(hash_t *hashes) {
    printf("\nCheck for reading beyond the end of the buffer and alignment problems. (Nothing but name is good.)\n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);

        uint8_t buf[MAXLEN+20], *b;
        uint32_t len;
        //uint8_t q[] = "This is the time for all good men to come to the aid of their country...";
        uint32_t h;
        //uint8_t qq[] = "xThis is the time for all good men to come to the aid of their country...";
        uint32_t i;
        //uint8_t qqq[] = "xxThis is the time for all good men to come to the aid of their country...";
        uint32_t j;
        //uint8_t qqqq[] = "xxxThis is the time for all good men to come to the aid of their country...";
        uint32_t ref,x,y;
        //uint8_t *p;

        // TODO I need to generalize this. Pff. - ABC
        /*
        printf("Endianness.  These lines should all be the same (for values filled in):\n");
        printf("%.8x                            %.8x                            %.8x\n",
             hashword((const uint32_t *)q, (sizeof(q)-1)/4, 13),
             hashword((const uint32_t *)q, (sizeof(q)-5)/4, 13),
             hashword((const uint32_t *)q, (sizeof(q)-9)/4, 13));
        p = q;
        printf("%.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x\n",
             hashlittle(p, sizeof(q)-1, 13), hashlittle(p, sizeof(q)-2, 13),
             hashlittle(p, sizeof(q)-3, 13), hashlittle(p, sizeof(q)-4, 13),
             hashlittle(p, sizeof(q)-5, 13), hashlittle(p, sizeof(q)-6, 13),
             hashlittle(p, sizeof(q)-7, 13), hashlittle(p, sizeof(q)-8, 13),
             hashlittle(p, sizeof(q)-9, 13), hashlittle(p, sizeof(q)-10, 13),
             hashlittle(p, sizeof(q)-11, 13), hashlittle(p, sizeof(q)-12, 13));
        p = &qq[1];
        printf("%.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x\n",
             hashlittle(p, sizeof(q)-1, 13), hashlittle(p, sizeof(q)-2, 13),
             hashlittle(p, sizeof(q)-3, 13), hashlittle(p, sizeof(q)-4, 13),
             hashlittle(p, sizeof(q)-5, 13), hashlittle(p, sizeof(q)-6, 13),
             hashlittle(p, sizeof(q)-7, 13), hashlittle(p, sizeof(q)-8, 13),
             hashlittle(p, sizeof(q)-9, 13), hashlittle(p, sizeof(q)-10, 13),
             hashlittle(p, sizeof(q)-11, 13), hashlittle(p, sizeof(q)-12, 13));
        p = &qqq[2];
        printf("%.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x\n",
             hashlittle(p, sizeof(q)-1, 13), hashlittle(p, sizeof(q)-2, 13),
             hashlittle(p, sizeof(q)-3, 13), hashlittle(p, sizeof(q)-4, 13),
             hashlittle(p, sizeof(q)-5, 13), hashlittle(p, sizeof(q)-6, 13),
             hashlittle(p, sizeof(q)-7, 13), hashlittle(p, sizeof(q)-8, 13),
             hashlittle(p, sizeof(q)-9, 13), hashlittle(p, sizeof(q)-10, 13),
             hashlittle(p, sizeof(q)-11, 13), hashlittle(p, sizeof(q)-12, 13));
        p = &qqqq[3];
        printf("%.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x %.8x\n",
             hashlittle(p, sizeof(q)-1, 13), hashlittle(p, sizeof(q)-2, 13),
             hashlittle(p, sizeof(q)-3, 13), hashlittle(p, sizeof(q)-4, 13),
             hashlittle(p, sizeof(q)-5, 13), hashlittle(p, sizeof(q)-6, 13),
             hashlittle(p, sizeof(q)-7, 13), hashlittle(p, sizeof(q)-8, 13),
             hashlittle(p, sizeof(q)-9, 13), hashlittle(p, sizeof(q)-10, 13),
             hashlittle(p, sizeof(q)-11, 13), hashlittle(p, sizeof(q)-12, 13));
        printf("\n");

        // check that hashlittle2 and hashlittle produce the same results
        i=47; j=0;
        hashlittle2(q, sizeof(q), &i, &j);
        if (hashlittle(q, sizeof(q), 47) != i) {
            printf("hashlittle2 and hashlittle mismatch\n");
        }

        // check that hashword2 and hashword produce the same results
        len = 0xdeadbeef;
        i=47, j=0;
        hashword2(&len, 1, &i, &j);
        if (hashword(&len, 1, 47) != i) {
            printf("hashword2 and hashword mismatch %x %x\n", i, hashword(&len, 1, 47));
        }
        */

        // check hash doesn't read before or after the ends of the string
        for (h=0, b=buf+1; h<8; ++h, ++b) {
            for (i=0; i<MAXLEN; ++i) {
                len = i;
                for (j=0; j<i; ++j) *(b+j)=0;

                // these should all be equal
                switch(hasher->funcType) {
                    case 0:
                        ref = hasher->func.hash0(b, len);
                        break;
                    case 1:
                        ref = hasher->func.hash1(b, len);
                        break;
                    case 2:
                        ref = hasher->func.hash2(b, len, 0);
                        break;
                    default:
                        fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                        exit(1);
                        break;
                }
                *(b+i)=(uint8_t)~0;
                *(b-1)=(uint8_t)~0;
                switch(hasher->funcType) {
                    case 0:
                        x = hasher->func.hash0(b, len);
                        y = hasher->func.hash0(b, len);
                        break;
                    case 1:
                        x = hasher->func.hash1(b, len);
                        y = hasher->func.hash1(b, len);
                        break;
                    case 2:
                        x = hasher->func.hash2(b, len, 0);
                        y = hasher->func.hash2(b, len, 0);
                        break;
                    default:
                        fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                        exit(1);
                        break;
                }
                if ((ref != x) || (ref != y)) {
                    printf("Alignment error: %.8x %.8x %.8x %d %d\n",ref,x,y,h,i);
                }
            }
        }
    }
}


//========================================================================
// Iterative hashing testing
//========================================================================
// I'm not interested in iterative hashing right now.
// It's here if you want it. - ABC
/*
void testIterative(hash_t *hashes) {
    printf("\nCheck for problems with iterative hashing\n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);

        uint8_t buf[1];
        uint32_t h,i,state[HASHSTATE];

        buf[0] = ~0;
        for (i=0; i<HASHSTATE; ++i) {
            state[i] = 1;
        }
        printf("These should all be different\n");
        for (i=0, h=0; i<8; ++i) {
            switch(hasher->funcType) {
                case 0:
                    h = hasher->func.hash0(buf, 0, h);
                    break;
                case 1:
                    h = hasher->func.hash1(buf, 0, h);
                    break;
                default:
                    fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                    exit(1);
                    break;
            }
            printf("%2ld  0-byte strings, hash is  %.8x\n", i, h);
        }

    }
}
*/

//========================================================================
// Correct values testing
//========================================================================
/* I guess this is checking against known values... Not worth generalizing I don't think. - ABC
void driver5() {
  uint32_t b,c;
  b=0, c=0, hashlittle2("", 0, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // deadbeef deadbeef
  b=0xdeadbeef, c=0, hashlittle2("", 0, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // bd5b7dde deadbeef
  b=0xdeadbeef, c=0xdeadbeef, hashlittle2("", 0, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // 9c093ccd bd5b7dde
  b=0, c=0, hashlittle2("Four score and seven years ago", 30, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // 17770551 ce7226e6
  b=1, c=0, hashlittle2("Four score and seven years ago", 30, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // e3607cae bd371de4
  b=0, c=1, hashlittle2("Four score and seven years ago", 30, &c, &b);
  printf("hash is %.8lx %.8lx\n", c, b);   // cd628161 6cbea4b3
  c = hashlittle("Four score and seven years ago", 30, 0);
  printf("hash is %.8lx\n", c);   // 17770551
  c = hashlittle("Four score and seven years ago", 30, 1);
  printf("hash is %.8lx\n", c);   // cd628161
}
*/


//========================================================================
// Distribution testing
//========================================================================
/*
#define PI 3.1415926535897932

double ChiSq(double x, int n) {
    double q, t, p;
    int k, a;

    if (n == 1 && x > 1000) return 0.0;

    if (x > 1000 || n > 1000) {
        q = ChiSq((x - n) * (x - n) / (2 * n), 1) / 2;
        if (x > n)
            return q;
        else
            return 1 - q;
    }

    p = exp(-0.5 * x);
    if ((n % 2) == 1) {
        p *= sqrt(2 * x / PI);
    }

    k = n;
    while (k >= 2) {
        p *= x / k;
        k -= 2;
    }

    t = p;
    q = -1;
    a = n;
    while (q != p) {
        a += 2;
        t *= x /a;
        q = p;
        p += t;
    }

    if (p > 1) p = 1;
    return 1 - p;
}
*/


/*
// TODO: Finish distribution test
void testDistribution(hash_t *hashes ) {
    printf("\nCheck for uniform distribution\n");
    uint32_t t_id;



    static unsigned char randomBuff[BUFF_SZ];
    randomBuff[0]=0;
    for (int i=1; i < BUFF_SZ; i++) { randomBuff[i] = (char) (i + randomBuff[i-1]); }


    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);

        GetRandomKey[] keyfunc = {
            new GetRandomKey(HashFunction.GetUniformKey),
            new GetRandomKey(HashFunction.GetTextKey),
            new GetRandomKey(HashFunction.GetSparseKey),
        };

        printf("      Uniform keys    Text keys     Sparse keys\n");
        printf("Bits  Lower  Upper   Lower  Upper   Lower  Upper");
        for (int bits=1; bits<=16; bits++) {
            printf("%u ", bits);
            foreach (GetRandomKey getKey in keyfunc) {
                printf(" ");
                uint mask = (1U << bits) - 1U;
                int E = 100;
                for (int lsb=0; lsb<32; lsb += 32-bits) {
                    if (hf is FNV) {
                        if (lsb == 0)
                            hf = new FNV(bits);
                        else
                            hf = new FNV();
                    }
                    int[] buckets = new int[1 << bits];
                    int n = E * buckets.Length;
                    for (int i=0; i<n; i++) {
                        byte[] key = getKey();
                        uint hash = hf.ComputeHash(key);
                        uint bucket = (hash >> lsb) & mask;
                        buckets[bucket]++;
                    }
                    double X = 0.0;
                    double p;
                    for (int i=0; i<buckets.Length; i++) {
                        double err = buckets[i] - E;
                        X += err * err / E;
                        p = buckets[i] / (double)n;
                    }
                    p = ChiSq(X, buckets.Length - 1);
                    printf("  {0:0.000}", p);
                }
            }
            printf("\n");
        }
    }
}
*/

//========================================================================
// Frog testing
//========================================================================
#define BITS   8                 // maximum number of bits set in the string
#define BYTES  24                // length of the string, in bytes
#define LARRAY 16                // 1<<LARRAY HLEN*ub4s per malloc
#define LMMM   8                 // 1<<LMMM mallocs total
#define HLEN   2                 // length of a hash value, in ub4s
#define ARRAY (((uint32_t)1)<<LARRAY) // length of a malloc, in ub4s
#define MMM   (((uint32_t)1)<<LMMM)   // number of segments in hash table

typedef struct {
    uint64_t    count;     // total number of strings tested
    uint32_t    a[BITS];   // a[i] says that q[a[i]] should be the ith word
    uint32_t   *hash[MMM]; // last such hash value
    unsigned char *string;    // string to be hashed
    uint32_t    mask;      // what section of values to look at
    hash_t *hasher; // Which hash function we're using
} mystate;

void frogCheck(mystate *state) {
    uint32_t  val;
    uint32_t  zip;
    uint32_t  off;
    uint32_t *where;
    uint32_t  hashval;
    ++state->count;

    // report how much progress we have made
    if (!(state->count & (state->count-1))) {
        int j;
        for (j=0; (((uint64_t)1)<<j) < state->count; ++j) {
        }
        printf("count 2^^%d, covered 2^^%d key pairs\n", j, j <= LARRAY+LMMM ? 2*j-1 : (LARRAY+LMMM) + j);
    }

    // compute a new hash value
    switch(state->hasher->funcType) {
        case 0:
            hashval = state->hasher->func.hash0(state->string, (BYTES/8));
            break;
        case 1:
            hashval = state->hasher->func.hash1(state->string, (BYTES/8));
            break;
        case 2:
            hashval = state->hasher->func.hash2(state->string, (BYTES/8), 0);
            break;
        default:
            fprintf(stderr, "Error: Unknown funcType %u for test with name %s\n", state->hasher->funcType, state->hasher->name);
            exit(1);
            break;
    }

    // look up where it belongs in the hash table
    val = hashval;
    zip = ((val>>LMMM)&(ARRAY-1))*HLEN;
    off = (val)&(MMM-1);
    where = &state->hash[off][zip];

    // did we actually get a collision?
    if (where[0] == hashval) {
        printf("collision!  hash value %08x for input %.2x %.2x %.2x at count %lu\n", where[0], state->string[0], state->string[1], state->string[2], state->count);
    }

    // store the current hash value
    where[0] = hashval;
}

void frogRecurse(int depth, mystate *state) {
    // set my bit
    state->string[state->a[depth]>>3] ^= (1<<(state->a[depth]&0x7));
    frogCheck(state);

    // should we set some more bits?
    if (depth+1 < BITS) {
        int newdepth = depth+1, i;
        for (i=state->a[depth]; i--;) {
            state->a[newdepth] = i;
            frogRecurse(newdepth, state);
        }
    }
    // clear my bit
    state->string[state->a[depth]>>3] ^= (1<<(state->a[depth]&0x7));
}


void testFrog(hash_t *hashes) {
    printf("\nCheck sparse bit key hashing\n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);

        mystate state;
        state.hasher = hasher;
        uint32_t *a = state.a;
        uint64_t s[(BYTES+7)/8];
        uint32_t i, j;

        // initialize the state
        for (i=0; i<(BYTES+7)/8; ++i) {
            s[i] = (uint64_t)0;
        }
        state.string = (unsigned char *)s;
        state.count  = (uint64_t)0;

        // allocate memory for the hash table
        for (i=0; i<MMM; ++i) {
            state.hash[i] = (uint32_t *)malloc(ARRAY*HLEN*sizeof(uint32_t));
            if (!state.hash[i]) printf("could not malloc\n");
            for (j = 0; j < ARRAY; ++j) {
                state.hash[i][j] = (uint32_t)0;          // zero out the hash table
            }
        }

        // do the work
        for (a[0] = 8*BYTES; a[0]--;) {
            frogRecurse(0, &state);
        }

        // free the hash table
        for (i=0; i<MMM; ++i) {
            free(state.hash[i]);
        }
    }
}

//========================================================================
// Numbers test
//========================================================================
void testNumbers(hash_t *hashes, int32_t len) {
    if (len < 1) {
        len = 16384;
        if (verbose) {
            len = 65536;
        }
        if (quiet) {
            len = 1024;
        }
    }
    printf("\nCheck hash values of raw numbers 0 through %u\n", len);
    uint32_t t_id;
    uint32_t result;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s", hasher->name);
        for (int i=0; i < len; ++i) {
            if (i%16==0) {
                printf("\n");
            }
            switch(hasher->funcType) {
                case 0:
                    result = hasher->func.hash0((unsigned char *)&i, (i > 255) ? 2 : 1);
                    break;
                case 1:
                    result = hasher->func.hash1((unsigned char *)&i, (i > 255) ? 2 : 1);
                    break;
                case 2:
                    result = hasher->func.hash2(&i, (i > 255) ? 2 : 1, 0);
                    break;
                default:
                    fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hasher->funcType, t_id, hasher->name);
                    exit(1);
                    break;
            }
            printf("%.8x ", result);

        }
        printf("\n");
    }
}

//========================================================================
// Dict test
//========================================================================
void *mem(size_t s) {
    void *ret;
    uint8_t retries=-1;
    while ((ret=malloc(s)) == NULL && retries-->0);
    if (ret == NULL) {
        fprintf(stderr, "Out of memory ");
    }
    return ret;
}

#define FREAD_BUF_SIZE 1024
unsigned char* readline(FILE *f) {
    if (f == NULL) { return NULL; }

    unsigned buff_len = 0;
    unsigned buff_cap = FREAD_BUF_SIZE;
    unsigned char *buff, *temp;

    buff = mem(sizeof(char)*FREAD_BUF_SIZE);
    if (buff == NULL) {
        fprintf(stderr, "readline() error: Out of (malloc) memory.");
        return NULL;
    }

    while (buff_len < buff_cap) {
        buff[buff_len] = fgetc(f);
        ++buff_len;

        if (buff[buff_len-1] == '\n' || feof(f)) {
            temp = realloc(buff, sizeof(char) * (buff_len+1));
            if (temp == NULL) {
                fprintf(stderr, "getfile() error: Out of (final realloc) memory.");
                free(buff);
                return NULL;
            }
            buff = temp;
			buff[buff_len] = 0; // null terminate the char *
			break;
        }

        if (ferror(f)) {
            fprintf(stderr, "getfile() error: ferror() while reading.");
            free(buff);
            return NULL;
        }

        // Ensure there is always at least FREAD_BUF_SIZE space to read into
        if ((buff_cap - buff_len) < FREAD_BUF_SIZE) {
            buff_cap = buff_len + (buff_cap - buff_len) + FREAD_BUF_SIZE;
            temp = realloc(buff, sizeof(char)*buff_cap);
            if (temp == NULL) {
				fprintf(stderr, "getfile() error: Out of (realloc) memory.");
				free(buff);
                return NULL;
            }
			buff = temp;
        }
    }

    return buff;
}

size_t ustrlen(const unsigned char *str) {
    const unsigned char *s;
    for (s = str; *s; ++s);
    return (s-str);
}

enum METHOD {
    UPPER_16,
    LOWER_16,
    MODULO,
};

void dictHash(char *filename, hash_t *hash, uint32_t hash_table_len, uint32_t collision_method) {
    uint32_t collision_table[hash_table_len];
    for (uint32_t i=0; i<hash_table_len; ++i) {
        collision_table[i] = 0;
    }

    // Open file
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        fprintf(stderr, "dictHash(%s) error: Cannot open file for hash %s\n", filename, hash->name);
        exit(1);
    }

    unsigned char *key;
    uint32_t len;
    uint32_t result;

    while (1) {
        key = readline(f);
        if (key == NULL) {
            fprintf(stderr, "Error testing dict hash with %s\n", hash->name);
            exit(1);
        }
        len = ustrlen(key);

        if (len < 1) {
            continue;
        }

        switch(hash->funcType) {
            case 0:
                result = hash->func.hash0(key, len);
                break;
            case 1:
                result = hash->func.hash1(key, len);
                break;
            case 2:
                result = hash->func.hash2(key, len, 0);
                break;
            default:
                fprintf(stderr, "Error: Unknown funcType %u with name %s\n", hash->funcType, hash->name);
                exit(1);
                break;
        }

        switch(collision_method) {
            case UPPER_16:
                collision_table[(result&0xFFFF0000)>>16] += 1;
                break;
            case LOWER_16:
                collision_table[result&0x0000FFFF] += 1;
                break;
            case MODULO:
                collision_table[result%hash_table_len] += 1;
                break;
            default:
                fprintf(stderr, "Error: Unknown funcType %u with name %s\n", hash->funcType, hash->name);
                exit(1);
                break;
        }

        if (feof(f)) {
            break;
        }
    }

    // Try to close until it closes or we give up
    int close_attempts = 10;
    while (fclose(f) != 0 && close_attempts > 0) {
        --close_attempts;
    };
    if (close_attempts <= 0) {
        fprintf(stderr, "getfile(%s) error: Cannot close. Proceeding obliviously.\n", filename);
    }

    uint32_t collision_count = 0;
    uint32_t max_collision = 0;
    uint32_t max_spot = 0;

    for (uint32_t i=0; i<hash_table_len; ++i) {
        if (collision_table[i] > 1) {
            ++collision_count;
            if (collision_table[i] > max_collision) { max_collision = collision_table[i]; max_spot = i; }
            if (verbose) printf("collision[%5u] %u   ", i, collision_table[i]);
            if (verbose && collision_count % 8 == 0) { printf("\n"); }
        }
    }
    if (verbose) printf("\n");
    printf("\t\tTotal %u -- Max %u at collision[%u]\n", collision_count, max_collision, max_spot);
}

void testDictModulo(hash_t *hashes) {
    printf("\nCheck hash collisions against dict.txt 50101 english+american words in a 65536 hash table using %% LEN \n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);
        dictHash("dict.txt", hasher, 65536, MODULO);
    }
}

void testDictUpper(hash_t *hashes) {
    printf("\nCheck hash collisions against dict.txt 50101 english+american words in a 65536 hash table using upper 16 bits \n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);
        dictHash("dict.txt", hasher, 65536, UPPER_16);
    }
}

void testDictLower(hash_t *hashes) {
    printf("\nCheck hash collisions against dict.txt 50101 english+american words in a 65536 hash table using lower 16 bits\n");
    uint32_t t_id;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\t%s\n", hasher->name);
        dictHash("dict.txt", hasher, 65536, LOWER_16);
    }
}

//========================================================================
// Images test
//========================================================================
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v ) {
    int i;
    float f, p, q, t;
    if( s == 0 ) {
        // achromatic (grey)
        *r = *g = *b = v;
        return;
    }
    h /= 60;            // sector 0 to 5
    i = floor( h );
    f = h - i;          // factorial part of h
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );
    switch( i ) {
        case 0:
            *r = v;
            *g = t;
            *b = p;
            break;
        case 1:
            *r = q;
            *g = v;
            *b = p;
            break;
        case 2:
            *r = p;
            *g = v;
            *b = t;
            break;
        case 3:
            *r = p;
            *g = q;
            *b = v;
            break;
        case 4:
            *r = t;
            *g = p;
            *b = v;
            break;
        default:        // case 5:
            *r = v;
            *g = p;
            *b = q;
            break;
    }
}

void ConvRGB32ToRGB24(const uint32_t *Src, uint32_t *Dst, uint32_t Pixels) {
    for (uint32_t i = 0; i < Pixels; i += 4) {
        uint32_t    sa = Src[i + 0] & 0xffffff;
        uint32_t    sb = Src[i + 1] & 0xffffff;
        uint32_t    sc = Src[i + 2] & 0xffffff;
        uint32_t    sd = Src[i + 3];
        Dst[0] = sa | (sb << 24);
        Dst[1] = (sb >> 8) | (sc << 16);
        Dst[2] = (sc >> 16) | (sd << 8);
        Dst += 3;
    }
}

float lerp(float v0, float v1, float t) {
  return (1-t)*v0 + t*v1;
}

#define my_round(x) ((x)>=0?(int32_t)((x)+0.5):(int32_t)((x)-0.5))

enum { 
    IMAGE_TYPE_CROWD = 1,
    IMAGE_TYPE_COLLISION = 2,
};

uint32_t get_crowd_color_ (uint32_t color) {
    switch (color) {
        case 0:
            return 0xffffff;
        case 1:
            return 0x33ff00;
        case 2:
            return 0x333333;
        case 3:
            return 0x676701;
        case 4:
            return 0xcccc00;
        case 5:
            return 0xffff33;
        case 6:
            return 0xffc803;
        case 7:
            return 0xff9900;
        case 8:
            return 0xff6600;
        case 9:
            return 0xcc3300;
        case 10:
            return 0xff3300;
        case 11:
            return 0xff0000;
        case 12:
            return 0xcc0000;
        case 13:
            return 0x990000;
        case 14:
            return 0xff0033;
        case 15:
            return 0xfe00cc;
        case 16:
            return 0xcc00cc;
        case 17:
            return 0x9901cc;
        case 18:
            return 0xff66ff;
        case 19:
        default:
            return 0x34ffff;
    }
}


uint32_t get_crowd_color (uint32_t color) {
    color = get_crowd_color_(color);
    uint32_t r,g,b;
    b = color & 0xff;
    color >>= 8;
    g = color & 0xff;
    color >>= 8;
    r = color & 0xff;

    return (b<<16) | (g<<8) | (r);
}

void dictHashImages(char *filename, hash_t *hash, uint32_t hash_table_len, int image_type) {
    uint32_t collision_table[hash_table_len];

    for (uint32_t i=0; i<hash_table_len; ++i) {
        collision_table[i] = 0;
    }
    
    // Open file
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        fprintf(stderr, "dictHash(%s) error: Cannot open file for hash %s\n", filename, hash->name);
        exit(1);
    }

    unsigned char *key;
    uint32_t len;
    uint32_t result;
    uint32_t current;
    double scale;
    int32_t success;
    char buf[4096];
    float r,g,b;
    uint32_t rr;
    uint32_t gg;
    uint32_t bb;
    char *image_name;

    while (1) {
        key = readline(f);
        if (key == NULL) {
            fprintf(stderr, "Error testing dict hash with %s\n", hash->name);
            exit(1);
        }
        len = ustrlen(key);

        if (len < 1) {
            continue;
        }

        switch(hash->funcType) {
            case 0:
                result = hash->func.hash0(key, len);
                break;
            case 1:
                result = hash->func.hash1(key, len);
                break;
            case 2:
                result = hash->func.hash2(key, len, 0);
                break;
            default:
                fprintf(stderr, "Error: Unknown funcType %u with name %s\n", hash->funcType, hash->name);
                exit(1);
                break;
        }
        current = result%hash_table_len;
        collision_table[current] += 1;

        if (feof(f)) {
            break;
        }
    }

    // Try to close until it closes or we give up
    int close_attempts = 10;
    while (fclose(f) != 0 && close_attempts > 0) {
        --close_attempts;
    };
    if (close_attempts <= 0) {
        fprintf(stderr, "getfile(%s) error: Cannot close. Proceeding obliviously.\n", filename);
    }


    switch(image_type) {
        case IMAGE_TYPE_CROWD:
           for (uint32_t i=0; i<hash_table_len; ++i) {
                collision_table[i] = get_crowd_color(collision_table[i]);
            }
            ConvRGB32ToRGB24(collision_table, collision_table, hash_table_len);
            image_name = "_dict_crowd.bmp";
            break;

        case IMAGE_TYPE_COLLISION:
            for (uint32_t i=0; i<hash_table_len; ++i) {
                if (collision_table[i] == 0) {
                    collision_table[i] = 0xFFFFFF;
                } else if (collision_table[i] >= 1) {
                    scale = current / hash_table_len;
                    scale = lerp(0.0, 360.0, scale);
                    HSVtoRGB(&r, &g, &b, scale, 1.0, 1.0);
                    bb = (uint32_t)round(lerp(0.0, 255.0, b));
                    gg = (uint32_t)round(lerp(0.0, 255.0, g));
                    rr = (uint32_t)round(lerp(0.0, 255.0, r));
                    collision_table[i] = (bb << 16) | (gg << 8) | rr;
                }
            }


            ConvRGB32ToRGB24(collision_table, collision_table, hash_table_len);
            image_name = "_dict_art.bmp";
            break;
   }

    double derp = sqrt(hash_table_len + 0.0);
    int w=ceil(derp);
    int h=ceil(derp);

    snprintf(buf, sizeof(buf), "%s%s", hash->name, image_name);
    success = stbi_write_bmp(buf, w, h, 3, collision_table);
    if (success == 0) {
        printf("ERROR ON IMAGE: %i\n", success);
    }


 /*
    uint32_t collision_count = 0;
    uint32_t max_collision = 0;
    uint32_t max_spot = 0;

    for (uint32_t i=0; i<hash_table_len; ++i) {
        if (collision_table[i] > 1) {
            ++collision_count;
            if (collision_table[i] > max_collision) { max_collision = collision_table[i]; max_spot = i; }
            if (verbose) printf("collision[%5u] %u   ", i, collision_table[i]);
            if (verbose && collision_count % 8 == 0) { printf("\n"); }
        }
    }
    if (verbose) printf("\n");
    printf("\t\tTotal %u -- Max %u at collision[%u]\n", collision_count, max_collision, max_spot);
*/

    if (verbose) { printf("\t%s %s\n", hash->name, buf); }
}


void testNumbersImages(hash_t *hashes, int32_t len, uint32_t hash_table_len, int image_type) {
    if (len < 1) {
        len = 16384;
        if (verbose) {
            len = 65536;
        }
        if (quiet) {
            len = 1024;
        }
    }
    uint32_t result;
    uint32_t collision_table[hash_table_len];
    for (uint32_t i=0; i<hash_table_len; ++i) {
        collision_table[i] = 0;
    }
    uint32_t current;
    double scale;
    int32_t success;
    char buf[4096];
    float r,g,b;
    uint32_t rr;
    uint32_t gg;
    uint32_t bb;
    char *image_name;

    //printf("\t%s", hashes->name);
    for (int i=0; i < len; ++i) {
        switch(hashes->funcType) {
            case 0:
                result = hashes->func.hash0((unsigned char *)&i, (i > 255) ? 2 : 1);
                break;
            case 1:
                result = hashes->func.hash1((unsigned char *)&i, (i > 255) ? 2 : 1);
                break;
            case 2:
                result = hashes->func.hash2(&i, (i > 255) ? 2 : 1, 0);
                break;
            default:
                fprintf(stderr, "Error: Unknown funcType %u for test %u with name %s\n", hashes->funcType, 9999999, hashes->name);
                exit(1);
                break;
        }

        current = result%hash_table_len;
        collision_table[current] += 1;

    }


    switch(image_type) {
        case IMAGE_TYPE_CROWD:
            for (uint32_t i=0; i<hash_table_len; ++i) {
                collision_table[i] = get_crowd_color(collision_table[i]);
            }
            ConvRGB32ToRGB24(collision_table, collision_table, hash_table_len);
            image_name = "_numbers_crowd.bmp";
            break;

        case IMAGE_TYPE_COLLISION:
            for (uint32_t i=0; i<hash_table_len; ++i) {
                if(collision_table[i] == 0) {
                    collision_table[i] = 0xFFFFFF;
                } else if (collision_table[i] >= 1) {
                    scale = current / hash_table_len;
                    scale = lerp(0.0, 360.0, scale);
                    HSVtoRGB(&r, &g, &b, scale, 1.0, 1.0);
                    bb = (uint32_t)round(lerp(0.0, 255.0, b));
                    gg = (uint32_t)round(lerp(0.0, 255.0, g));
                    rr = (uint32_t)round(lerp(0.0, 255.0, r));
                    collision_table[i] = (bb << 16) | (gg << 8) | rr;
                }
            }
            ConvRGB32ToRGB24(collision_table, collision_table, hash_table_len);
            image_name = "_numbers_collision.bmp";
            break;
    }

    double derp = sqrt(hash_table_len + 0.0);
    int w=ceil(derp);
    int h=ceil(derp);
    snprintf(buf, sizeof(buf), "%s%s", hashes->name, image_name);
    success = stbi_write_bmp(buf, w, h, 3, collision_table);
    if (success == 0) {
        printf("ERROR ON IMAGE: %i\n", success);
    }
    if (verbose) { printf("\t%s %s\n", hashes->name, buf); }
}

void testImages(hash_t *hashes) {
    printf("\nCheck hash collisions and randomeness with images\n");
    uint32_t t_id;
    uint32_t len;
    uint32_t hash_table_len;

    for (t_id=0; hashes[t_id].name != NULL; t_id++) {
        hash_t *hasher = &hashes[t_id];
        printf("\n\t%s", hasher->name);
        //int stbi_write_bmp(char const *filename, int w, int h, int comp, const void *data);
        // SPARSE 50101 dict /(344*344) test| 0.42etc
        //dictHashImages("dict_sparse.txt", hasher, 118336);
        // FULL 253k (724x724) | 0.4something
        hash_table_len = 524176;
        if (verbose) { printf("\t\tCheck hash values of dictionary english terms of %u in table of size %u\n", 235924, hash_table_len); }
        dictHashImages("dict.txt", hasher, hash_table_len, IMAGE_TYPE_CROWD);
        dictHashImages("dict.txt", hasher, hash_table_len, IMAGE_TYPE_COLLISION);

        // 216553 numbers /(724*724) 524176 test | 0.41199222257
        len = 216553;
        hash_table_len = 524176;
        if (verbose) { printf("\t\tCheck hash values of raw numbers 0 through %u in table of size %u\n", len, hash_table_len); }
        testNumbersImages(hasher, len, hash_table_len, IMAGE_TYPE_CROWD);
        testNumbersImages(hasher, len, hash_table_len, IMAGE_TYPE_COLLISION);
        //dictHashImages("dict.txt", hasher, 524176)
    }
}



//========================================================================
// Test driving
//========================================================================
enum HashFunctions {
    H_CRC32       = 1u << 0,
    H_ONEATATIME  = 1u << 1,
    H_ALPHANUM    = 1u << 2,
    H_FNV         = 1u << 3,
    H_BOBJENKINS  = 1u << 4,
    H_SUPERFAST   = 1u << 5,
    H_ABC         = 1u << 6,
    H_ABC64       = 1u << 7,
    H_BOBJENKINS2 = 1u << 8,
};

enum HashTests {
    T_SPEED     = 1u << 0,
    T_SPEED2    = 1u << 1,
    T_AVALANCHE = 1u << 2,
    T_BOUNDS    = 1u << 3,
    T_NUMS      = 1u << 4,
    T_FROG      = 1u << 5,
    T_DICTMOD   = 1u << 6,
    T_DICTUPPER = 1u << 7,
    T_DICTLOWER = 1u << 8,
    T_IMAGES = 1u << 9,
};

void helpString(hash_t *hashes) {
    printf("OVERVIEW: hashtest: Tests specified hashes with specified tests.\n\n");
    printf("USAGE: hashtest [options]\n\n");
    printf("OPTIONS:\n");
    printf("\t-h, --help   Print this help message.\n");
    printf("\t-v, verbose mode, more info from tests.\n");
    printf("\t-q, quieter mode, less info from tests.\n");
    printf("\n\tHash specifiers. Must use at least one.\n");
    printf("\t--all          (Run all hashes)\n");
    for (uint32_t h_id=0; hashes[h_id].name != NULL; h_id++) {
        hash_t *hasher = &hashes[h_id];
        printf("\t--%s\n",hasher->name);
    }
    printf("\n\tTest specifiers. Must use at least one.\n");
    printf("\t--allTests      Runs ALL tests\n");
    printf("\t--testSpeed     Tests hash speed with random data.\n");
    printf("\t--testSpeed2    Tests hash speed with constant data.\n");
    printf("\t--testAvalanche Tests hash ability to make all bits affected by small key changes.\n");
    printf("\t--testBounds    Tests that hashes don't read outside the key buffers.\n");
    printf("\t--testNums      Tests hash ability to cope with raw numeric data.\n");
    printf("\t--testFrog      Tests hash ability to cope with sparse bit keys. (I think this is broken. :|)\n");
    printf("\t--testDictMod   Tests hash collisions against a dictionary file of 50101 english words\n\t\t\tdict.txt is used, generated by scowl with ./mk-list english american 35\n");
    printf("\t--testDictUpper Tests hash collisions against same dict, using upper 16 bits of hash\n");
    printf("\t--testDictLower Tests hash collisions against same dict, using lower 16 bits of hash\n");
    printf("\t--testImages Tests hash collision and randomness with images output\n");


}

#define NUM_HASHES 10
#define NUM_TESTS 9

int main(int argc, char *argv[]) {
    hash_t hashes[NUM_HASHES];
    hashes[0].funcType = 0; hashes[0].func.hash0 = GetCRC32;       hashes[0].maskValue = H_CRC32;       hashes[0].name = "CRC32";
    hashes[1].funcType = 0; hashes[1].func.hash0 = oneAtATimeHash; hashes[1].maskValue = H_ONEATATIME;  hashes[1].name = "OneAtATime";
    hashes[2].funcType = 0; hashes[2].func.hash0 = alphaNumHash;   hashes[2].maskValue = H_ALPHANUM;    hashes[2].name = "AlphaNum";
    hashes[3].funcType = 0; hashes[3].func.hash0 = FNVHash;        hashes[3].maskValue = H_FNV;         hashes[3].name = "FNV";
    hashes[4].funcType = 0; hashes[4].func.hash0 = hashBobJenkins; hashes[4].maskValue = H_BOBJENKINS;  hashes[4].name = "BobJenkins";
    hashes[5].funcType = 0; hashes[5].func.hash0 = SuperFastHash;  hashes[5].maskValue = H_SUPERFAST;   hashes[5].name = "SuperFast";
    hashes[6].funcType = 1; hashes[6].func.hash1 = ABCHash;        hashes[6].maskValue = H_ABC;         hashes[6].name = "ABC";
    hashes[7].funcType = 1; hashes[7].func.hash1 = ABCHash64;      hashes[7].maskValue = H_ABC64;       hashes[7].name = "ABC64";
    hashes[8].funcType = 2; hashes[8].func.hash2 = hashBobJenkins2;hashes[8].maskValue = H_BOBJENKINS2; hashes[8].name = "BobJenkins2";
    hashes[9].funcType = 0; hashes[9].func.hash0 = NULL;           hashes[9].maskValue = 0;             hashes[9].name = NULL;

    if (argc <= 1) {
        helpString(hashes);
        exit(0);
    }

    uint64_t hashes_to_run = 0;
    uint64_t tests_to_run = 0;
    int do_all_hashes=0;
    int do_all_tests=0;

    uint32_t h_id;

    for (int curr_arg=1; curr_arg < argc; ++curr_arg) {
        int do_continue=0;

        // Check for a hash run argument
        for (h_id=0; hashes[h_id].name != NULL; h_id++) {
            hash_t *hasher = &hashes[h_id];
            size_t name_len = strlen(hasher->name);

            char expected_arg[name_len+3];
            expected_arg[0] = '-';
            expected_arg[1] = '-';
            expected_arg[2] = 0;
            strcat(expected_arg, hasher->name);
            //printf("Looking for \"%s\"\n", expected_arg);
            if (strcmp(expected_arg, argv[curr_arg]) == 0) {
                hashes_to_run |= hasher->maskValue;
                do_continue = 1;
                break;
            }
        }

        if (do_continue) {
            continue;
        }

        // Check for a test run argument
        if (strcmp("--testSpeed", argv[curr_arg]) == 0) {
            tests_to_run |= T_SPEED;
            continue;
        }
        if (strcmp("--testSpeed2", argv[curr_arg]) == 0) {
            tests_to_run |= T_SPEED2;
            continue;
        }
        if (strcmp("--testAvalanche", argv[curr_arg]) == 0) {
            tests_to_run |= T_AVALANCHE;
            continue;
        }
        if (strcmp("--testBounds", argv[curr_arg]) == 0) {
            tests_to_run |= T_BOUNDS;
            continue;
        }
        if (strcmp("--testNums", argv[curr_arg]) == 0) {
            tests_to_run |= T_NUMS;
            continue;
        }
        if (strcmp("--testFrog", argv[curr_arg]) == 0) {
            tests_to_run |= T_FROG;
            continue;
        }
        if (strcmp("--testDictMod", argv[curr_arg]) == 0) {
            tests_to_run |= T_DICTMOD;
            continue;
        }
        if (strcmp("--testDictUpper", argv[curr_arg]) == 0) {
            tests_to_run |= T_DICTUPPER;
            continue;
        }
        if (strcmp("--testDictLower", argv[curr_arg]) == 0) {
            tests_to_run |= T_DICTLOWER;
            continue;
        }
        if (strcmp("--testImages", argv[curr_arg]) == 0) {
            tests_to_run |= T_IMAGES;
            continue;
        }


        if (strcmp("--all", argv[curr_arg]) == 0) {
            do_all_hashes = 1;
            continue;
        }

        if (strcmp("--allTests", argv[curr_arg]) == 0) {
            do_all_tests = 1;
            continue;
        }

        if (strcmp("-v", argv[curr_arg])==0) {
            verbose = 1;
            continue;
        }

        if (strcmp("-q", argv[curr_arg])==0) {
            quiet = 1;
            continue;
        }

        // Check for help stuff
        if (strcmp("-h", argv[curr_arg])==0 || strcmp("--help", argv[curr_arg])==0) {
            helpString(hashes);
            exit(0);
        }

        // If we get this far, unrecognized argument.
        fprintf(stderr, "Warning, unrecognized argument: %s\n", argv[curr_arg]);
    }

    uint32_t hashes_set = hashes_to_run;
    uint32_t hashes_count;
    // Count set bits in hashes bitmask
    for (hashes_count = 0; hashes_set; hashes_count++) {
        hashes_set &= hashes_set - 1; // clear the least significant bit set
    }
    if (do_all_hashes) { hashes_count = NUM_HASHES-1; }

    uint32_t tests_set = tests_to_run;
    uint32_t tests_count;
    // Count set bits in tests bitmask
    for (tests_count = 0; tests_set; tests_count++) {
        tests_set &= tests_set - 1; // clear the least significant bit set
    }
    if (do_all_tests) { tests_count = NUM_TESTS; }

    if (hashes_count < 1 && do_all_hashes == 0) {
        printf("Error: No hashes specified\n\n");
        helpString(hashes);
        return 1;
    }

    if (tests_count < 1 && do_all_tests != 0) {
        printf("Error: No tests specified\n\n");
        helpString(hashes);
        return 1;
    }

    // Create our specified list of hashes
    hash_t rhashes[hashes_count+1];
    uint32_t curr_rhash=0;
    for (h_id=0; hashes[h_id].name != NULL; h_id++) {
        hash_t *hasher = &hashes[h_id];
        if (hashes_to_run & hasher->maskValue || do_all_hashes) {
            rhashes[curr_rhash++] = *hasher;
        }
    }

    if (curr_rhash != hashes_count) {
        fprintf(stderr, "Error: Did not parse specified hashes correctly. Derp. %u %u", curr_rhash, hashes_count);
        exit(1);
    }

    rhashes[curr_rhash] = hashes[NUM_HASHES-1]; // Make sure to use the null entry

    // Get ready to test hashes!
    GenerateCRC32Table();

    if (tests_to_run & T_AVALANCHE || do_all_tests) { testAvalanche(rhashes); }
    if (tests_to_run & T_BOUNDS || do_all_tests) { testBoundsAndAlignment(rhashes); }
    if (tests_to_run & T_NUMS || do_all_tests) { testNumbers(rhashes, -1); }
    if (tests_to_run & T_FROG) { testFrog(rhashes); } // Must explicitly frog test. >:|
    if (tests_to_run & T_DICTMOD || do_all_tests) { testDictModulo(rhashes); }
    if (tests_to_run & T_DICTUPPER || do_all_tests) { testDictUpper(rhashes); }
    if (tests_to_run & T_DICTLOWER || do_all_tests) { testDictLower(rhashes); }
    if (tests_to_run & T_SPEED || do_all_tests) { testSpeed(rhashes); }
    if (tests_to_run & T_SPEED2 || do_all_tests) { testSpeed2(rhashes); }
    if (tests_to_run & T_IMAGES || do_all_tests) { testImages(rhashes); }
	return 0;
}
